# Koha Patron Image Packer

datalinker.ps1 is a tiny utility written using Microsoft PowerShell that allows users working on Windows based system to build their bulk patron image batch loader zip file along with it's IDLINK.txt manifest with just a single click.

## Getting Started

### Option #1

If you are **git** user on Windows, then clone this repo.

```
git clone https://gitlab.com/indradg/koha-patron-image-packer.git
```

### Option #2

Open the file *datalinker.ps1* in raw mode by [visiting this link](https://gitlab.com/indradg/koha-patron-image-packer/raw/master/datalinker.ps1) and save it on your Windows PC.

### Prerequisites

PowerShell 3.0 and .NET Framework 4.0 are the recommended minimum. You should be able to find this combination on the following Windows versions:

| .NET Framework 4.0 | Windows XP (SP3) | Windows 7 (SP1) | Windows 8 | Windows 8.1 | *or higher* |
|--------------------|------------------|-----------------|-----------|-------------|-------------| 


| Powershell 3.0     | Windows XP (SP3) | Windows 7 (SP1) | Windows 8 | Windows 8.1 | *or higher* |
|--------------------|------------------|-----------------|-----------|-------------|-------------| 

### Installing

Installation is nothing more than copying the datalinker.ps1 file over to folder which is holding your patron images named as per their cardnumbers in Koha.

However, before your Windows system may allow you to run an PowerShell script, it may be require you to set your **ExecutionPolicy** to *RemoteSigned* for your CurrentUser.

```
PS C:\> Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy RemoteSigned
```

To check if this has been set currently you can run the command and check the output.

```
PS C:> Get-ExecutionPolicy -List

                                            Scope                  ExecutionPolicy
                                            -----                  ---------------
                                    MachinePolicy                        Undefined
                                       UserPolicy                        Undefined
                                          Process                        Undefined
                                      CurrentUser                     RemoteSigned
                                     LocalMachine                        Undefined
```

## Demonstration

A screencast of this feature at work is available [here on Youtube](https://www.youtube.com/watch?v=Fly5JiF7ofw).

## Version

Version 0.1. Check the GitLabs repo for updates.

## Authors

* **Indranil Das Gupta** Founder, [L2C2 Technologies](http://www.l2c2.co.in)

## License

This project is licensed under the GPL v3 or later License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* A.J. Tomson, Librarian, Devagiri College, Kozhikode who wrote an early version of [analogous script in Python](https://docs.google.com/file/d/0B6E3iEzJp74uWWI4V3BwdUppdk0/edit?usp=sharing) in Koha 3.12 days.
* Vimal Kumar V. whose blog post [A useful script to compress patron images to upload](http://kohageek.blogspot.com/2013/08/a-useful-script-to-compress-patron.html) had originally informed me of Mr. Tomson's work.
* The [Koha ILS community](https://koha-community.org) who keep making it an even more awesome open source product with every release.