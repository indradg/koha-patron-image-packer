# --------------------------------------------------------------------------------
# Script    : DataLinker.ps1 (v 0.1)
# Author    : Indranil Das Gupta <indradg@l2c2.co.in>
# Copyright : L2C2 Technologies
# License   : GPL v3 or later
# Remarks   : Requires PS v3 and .NET Framework 4.0 or later
# --------------------------------------------------------------------------------
#
# get current folder location

$curr_folder = Get-Location

$date = Get-Date

$date = $date.ToString("yyyy-MM-dd")

$tmp_folder =  "C:\PATRON-IMAGE_$date" 

Write-Host "`nGetting image files from $curr_folder`n"

$gfx_files = Get-ChildItem $curr_folder\* -Include *.gif, *.jpg, *.jpeg, *.png, *.GIF, *.JPG, *.JPEG, *.PNG, *.xpm

$count_of_files = Get-ChildItem $curr_folder\* -Include *.gif, *.jpg, *.jpeg, *.png, *.GIF, *.JPG, *.JPEG, *.PNG, *.xpm | Measure-Object | %{$_.Count};

IF ($count_of_files -eq 0){

    Write-Host "No *.gif, *.jpg, *.jpeg, *.png, *.GIF, *.JPG, *.JPEG, *.PNG, *.xpm files found. Exiting now."

} else {

    # sanity check do not over-write existing upload ready archive

    IF ( Test-Path "$curr_folder\PATRON-IMAGE_$date.zip" -PathType Leaf ) {
        Write-Host "Image uploader zip PATRON-IMAGE_$date.zip already exists. Exiting!`n" 
        EXIT
    }

    $count_n = 0

    foreach($gfx_file in $gfx_files)
    {
        New-Item -ItemType directory -Path $tmp_folder -Force | Out-Null
        
        $card_ref = [System.IO.Path]::GetFileNameWithoutExtension($gfx_file)

        $file_ext = [System.IO.Path]::GetExtension($gfx_file)

        $a+=[Array] "$card_ref`t$card_ref$file_ext"

        Copy-Item "$curr_folder\$card_ref$file_ext" -destination $tmp_folder

        $count_n++

        Write-Host "Adding $card_ref$file_ext [$count_n of $count_of_files]`r"
    }

    $a | Out-File -filepath $tmp_folder\IDLINK.txt -Encoding ASCII 

    Write-Host "`nIDLINK.txt manifest file built`n"

    $compressionto_use = [System.IO.Compression.CompressionLevel]::Optimal

    $include_base_folder = $false

    $zip_to = "{0}\PATRON-IMAGE_{1}.zip" -f $curr_folder,$date

    #add the files in the zip archive

    Write-Host "Creating $zip_to.`n`nDepending on number and size of files this may take some time. Please be patient.`n"
   
    [Reflection.Assembly]::LoadWithPartialName( "System.IO.Compression.FileSystem" ) | Out-Null

    [System.IO.Compression.ZipFile]::CreateFromDirectory($tmp_folder, $zip_to)

    Write-Host "PATRON-IMAGE_$date.zip created.`n"
 
    Remove-Item $tmp_folder -RECURSE 
  
    Write-Host "Cleaning up and exiting.`n"

}

